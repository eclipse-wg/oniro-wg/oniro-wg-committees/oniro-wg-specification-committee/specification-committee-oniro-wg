# Oniro Working Group Specification Committee Meeting 2022wk47

MoM Status:  not approved

[comment]: # (approved by the Oniro WG Specification Committee on YYYY-MM-DD)

* Date: 2022-11-22
* Time: From 17:00 to 18:00 CEST
* Chair: Marta R.
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approval of the meeting minutes from the 2022-08-16 2022wk33 Oniro WG Specification Meeting - Marta R. (5 min)
* 2023 Project Plan and impact on specifications - all (30 min)
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

* Marta Rybczynska - Representing Committers
[comment]* Davide Ricci - Huawei
* Andrea Basso - Synesthesia

Other Attendees:

* none

Eclipse Foundation:

* none

[comment]: # ( * Wayne Beaton)
[comment]: # Agustin Benito Bethencourt
[comment]: # ( * Paul Buck)

## Minutes

### Approval of the meeting minutes from the 2022-08-16 2022wk33 Oniro WG Specification Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/oniro-working-group-specification-committee-meeting-2022wk33.md>

Commit considered: 9a857c24fd7dc31f1ea2aea0da7b40877a6465b7

### 2023 Project Plan and impact on specifications

* Discuss what the 2023 Project Plan means for the Specification Commitee activities

Marta has sent a new call for participation in specification projects. She proposes to freeze the committee work until there are volounteers and they create a specification project proposal.

The proposal has been approved unanimously.

### Resolutions

Approved unanimously the 2022wk33 MoM sha 9a857c24fd7dc31f1ea2aea0da7b40877a6465b7.

Freezed the Specification Committee activities until a specification proposal is created.

#### AoB  (Open Floor)

## Links and references

### Links

This is the list of relevant links:
* Oniro WG SpecC MoM: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/tree/main/agenda-mom-specification-committee-oniro-wg>
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at <17:30>
