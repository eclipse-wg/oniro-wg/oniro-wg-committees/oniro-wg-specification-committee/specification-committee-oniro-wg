# Oniro Working Group Specification Committee Meeting 2022wk25

MoM Status: approved on 2022-07-05 commit 40d5f7edcc2026b6a47ab48a874eee23f48ca1a3

* Date: 2022-06-21
* Time: From 17:00 to 18:00 CEST
* Chair: Marta Rybczynska 
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89291855112?pwd=UEpwNFZMV01tTkJHYzFEZ0Z6NEZjdz09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approve the MoM from 2022-06-07 2022wk23 - Marta R. - 5 minutes
* Jakarta as example for Oniro - TBD - 25 minutes
* First specification project: defining the subject. Marta R. - 15 minutes
* Availability of the Committee Members on summer. - Marta - 5 min.
* Resolutions - Andrea B. - 5 minutes
* AoB - Marta R. - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

[comment]: # (* Davide Ricci - Huawei)
* Marta Rybczynska - Representing Committers
* Andrea Basso - Synesthesia

[comment]: # (* Henk Veldhuis - Huawei alternate)

Oniro Specification Committee Regrets:

* Davide Ricci - Huawei

Other Attendees:

* none

Eclipse Foundation:

* Wayne Beaton
* Agustin Benito Bethencourt

## Minutes

### Approve the MoM from 2022-06-07 2022wk23

Link to the MoM #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/oniro-working-group-specification-committee-meeting-2022wk23.md>

* Approved unanimously.

### Jakarta as example for Oniro

postponed to next TBD

### First specification project: defining the subject.

#### Description

The goal is to guide the committers of the future first initial Specification Project in defining the subject so they can better describe the project proposal that will be submitted to EF.

We will:
* Define the subject list and prioritise it.
* For each highest priority subject, define the scope, stakeholders, involved.
* Who could be the initial spec project proposal writers and committers?

The expectation is to reach consensus on the above topics on the 2022wk27 (July 5th) session

#### Discussion
* Marta did introduction on the specification process. 
* Andrea discussed the specification list and the need to sync with other working groups 
* Agustin explained possible methods to address the issue:
- via open specification mailing list
- Introduce this point in the All Member reps. section of the Oniro WG SC. 
- ask directly to dev mailing list, committers, ..

Plan
* Announce in the oniro-wg and oniro-dev mailing lists that this process will start and that it will take place in the oniro-wg specification mailing list. Call for subscribing to that list.
* Create a topics proposal template.
   * It will be worked out through the oniro-wg-spec-committee mailing list.
   * It will include:
      - scope
      - short description
      - objective
      - harmonization with current status of the project
* A message asking for input will be sent to the oniro-wg-specification mailing list.
* Schedule a meeting to support the asynchronous process so proposers can discuss their proposals and ideas.
   * A meeting will be scheduled the 29th of June at 14 hours

The expectation is to collect all the input on the above topics by the 2022wk27 (July 5th) session

### Availability of the Committee Members on summer.

#### Description

Are the Oniro WG SpecC Members available during summer? If not, should we cancel any of the expected bi-weekly sessions? Which ones? 

#### Discussion

- Andrea will not available in August
- Marta will not be available in October
- Marta will ask Davide his Availability
- EF won't be available on week 28. In July the availability is reduced. Agustin will join in July.


### Resolutions

* Approved the MoM from 2022wk23 


#### AoB  (Open Floor)

* Agustin will confirm this week if the postponed topic today could be included in the agenda for the coming session.

## Links and references

### Links

This is the list of relevant links:
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>
* Oniro WG Specification Committee repository #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at 17:56
