# Oniro Working Group Specification Committee Meeting 2022wk29

MoM Status:  approved by the Oniro WG Specification Committee on 2022-08-02 commit SHA fccfe881c301d3f4aff3addfe8b87a2874455d6d

* Date: 2022-07-19
* Time: From 17:00 to 18:00 CEST
* Chair: Marta R.
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approval of the meeting minutes from the 2022-07-05 2022wk27 Oniro WG Specification Meeting - Marta R. (5 min)
* Potential first spec project. - Marta R and Davide R (20 min)
* Meta-subjects (general for all specifications) - Marta R and all (20 min)
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - (Huawei)
* Marta Rybczynska - (Representing Committers)
* Andrea Basso - (Synesthesia)

[comment]: # (* Francesco Ronchi - Synesthesia alternate)
* Henk Veldhuis - Huawei alternate - joined later

Oniro Specification Committee Regrets:



Other Attendees:

* none

Eclipse Foundation:

* Wayne Beaton - joined later
* Agustin Benito Bethencourt

[comment]: # ( * Paul Buck)

## Minutes

### Approval of the meeting minutes from the 2022-07-05 2022wk27 Oniro WG Specification Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/mom-oniro-working-group-specification-committee-2022wk27.md>

Commit considered: c0e05d8d0a8feb207e7a740088635ce38d5c5459

No objections, approved.


### Potential first spec project. - Marta R and Davide R (20 min)

Oniro Compatibility Specification - proposal available on the mailing list #link <https://www.eclipse.org/lists/oniro-wg-specification/msg00009.html>

Title: Oniro Compatibility Specification

Description: This specification defines requirements a product must meet to be called Oniro-compatible. The specification will include subjects like hardware features, BSP implementation, communication protocols to access other Oniro devices, compatibility with other standards, and vendor's processes. The specification might define profiles for different device types.

Andrea suggested:
a) I would distinguish between an Oniro component (i.e. an AI processing add-on that cannot work alone  vs a product that can work alone and  has a set of functions that can offer to other Oniro devices to create a "super-device”   
b) in terms of requirements I would add security support (hw, sw) that can be also part of the BSP,  communication hw support  (protocols can be part of the OS stack) and AI support


Scope: In-scope: allowing a certification of an end-product working with other Oniro devices.

Initial team (ideas!): someone from marketing (Phil, Chiara?), Davide, Amit/Andrei, at least one person from hardware vendors eg. Seco (Ettore?), Esben (or someone else from the OH effort), someone from OpenAtom?, Andrea with his previous spec experience (if he has time), Marta


### Meta-subjects (general for all specifications) - Marta R and all (20 min)

During last meeting we mentioned general subjects we should cover for all specifications:
- handling interoparability (automatic testing? testing with a device pool?)
- document format (markdown?)

### Resolutions

* Approval of agenda of previous meeting 2022wk27

#### AoB  (Open Floor)


## Links and references

### Links

This is the list of relevant links:
* Oniro WG SpecC MoM: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/tree/main/agenda-mom-specification-committee-oniro-wg>
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at <18:00>
