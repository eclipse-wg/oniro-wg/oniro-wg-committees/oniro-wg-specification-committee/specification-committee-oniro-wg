# Oniro Working Group Specification Committee Meeting 2022wk23

MoM Status:  approved on 2022-06-21 5c8e97d1

* Date: 2022-06-07
* Time: From 17:00 to 18:00 CEST
* Chair: Agustin Benito Bethencourt
* Minutes taker: Agustin Benito Bethencourt
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Welcome - Agustin 5 minutes
* Selection of Chair and Minutes taker - Agustin 5 minutes
* Schedule of the meeting series - Agustin 5 minutes
* 30, 60, 90 days plan - Wayne Beaton 20 minutes 
* Potential first spec project. - 15 min
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Marta Rybczynska - Representing Committers
* Francesco Ronchi - Synesthesia alternate

* Henk Veldhuis - Huawei alternate

Oniro Specification Committee Regrets:

* Andrea Basso - Synesthesia

Other Attendees:

* none

Eclipse Foundation:

* Wayne Beaton
* Agustin Benito Bethencourt

## Minutes

### Welcome

* Introduction of the participants (round robin) 


* Davide Ricci representing Huawei (Strategic Member)

Members of the committee as result of the elections:
* Andrea Basso, representing Synesthesia (Silver Members) 
* Marta Rybczynska, representing Committers

Alternates
* Henk Veldhuis - Huawei alternate
* Francesco Ronchi - Synesthesia alternate

### Selection of Chair and Minutes taker 

Candidates:
* Andrea B. sugested Davide through mailing.
* Davide R. suggests Marta. Marta volunteers.
* Francesco R. supports Marta.

Marta is elected as Chair. This is by default a 1 year position.

Davide suggest Andrea B.. Francesco accepts the role. Andrea Basso elected as Minutes Taker. This is a 1 year position. 


### Schedule of the meeting series

Andrea Basso proposed this same slot. Francesco R. supports this slot.
Davide R. supports this slot.
Marta supports.

Unanimously support for bi-weekly meetings.

This slot works for Wayne too.

### 30, 60, 90 days plan

This is the plan:

#### 30 days

In the first 30 days...

Establish committee membership as defined by the working group's charter, for example:
* Appoint Strategic Member representatives
* Appoint PMC representative
* Call for nominees and initiate elections for elected positions

Inaugural meeting
* EMO presentation on "how to operate a specification committee"
* Identify chairperson and secretary
* Establish initial policies regarding standing items, dissemination of minutes, use of public/private channels, etc.
* Set up mailing lists and other committee resources (e.g., GitLab repository)

#### 60 days

In the next 30 days...

* Establish meeting cadence
* Ballots to create one or more Specification Projects

#### 90 days

In the next 30 days (although likely somewhat longer term)...

* Ballots to approve Release Reviews for one or more Specification Projects
* Ratify Specification Version into a Final Specification

Thereafter...

* Ballots to approve Plan Reviews for subsequent releases
* Ballots to approve Release Reviews
* Ballots to approve Progress Reviews as required
* Ballots to approve Creation Reviews for new Specification Projects

Feedback to this plan can be provided [through Github](https://github.com/EclipseFdn/EFSP/issues/69)

Discussion

* PMC role and the convenience to have them involved (affects the Oniro WG Charter). 
* Jakarta EE can be taken as example of mature and active specifications. They have the notion of profiles which is related to branding strategies. Designating profiles is in the scope of this committee.
   * It can be interesting to bring them and learn how they do it.
* Discussion about profiles can be a good one to have.

### Potential first spec project.

* Davide: Oniro compatible specification for device makers seems like an obvious choice.
   * Thoughts about the complexity this could be.
* Marta adds to the above OTA (over the air updates) as example (in combination or individual).
* As follow up we can create a list of synergies we would like to create with other projects. Later we can evaluate if we can and should create specs with them.
* We need a timeline for 2022-2023 (early) 

### Resolutions

* Oniro Specification Committee Chair: Marta R.
* Oniro Specification Committee Minutes taker: Andrea Basso.
* Schedule: bi-weekly. Tuesday at 17:00 CEST 

#### AoB  (Open Floor)

* Remind the Oniro Members how important it is to show up in the committees 
* Answering Agustin question, Wayne recommends to start small.

## Links and references

### Links

This is the list of relevant links:
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at (18:03)
