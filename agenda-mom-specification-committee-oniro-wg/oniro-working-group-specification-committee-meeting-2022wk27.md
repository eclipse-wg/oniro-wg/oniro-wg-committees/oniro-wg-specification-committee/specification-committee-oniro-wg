# Oniro Working Group Specification Committee Meeting 2022wk27

MoM Status:  not approved

[comment]: # (approved by the Oniro WG Specification Committee on YYYY-MM-DD)

* Date: 2022-07-05
* Time: From 17:00 to 18:00 CEST
* Chair: Marta R.
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approval of the meeting minutes from the 2022-06-21 2022wk25 Oniro WG Specification Meeting - Marta R. (5 min)
* Potential first spec project. - Marta R and all (15 min)
* Jakarta as example for Oniro - Paul Buck 20 minutes (15 + 5Q&A)
* Confirm availability during summer of the Oniro WG SpecC Member Reps - Marta R. (5 min)
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

[comment]: # (* Davide Ricci - Huawei)
* Marta Rybczynska - Representing Committers
 * Andrea Basso - Synesthesia
[comment]: # (* Francesco Ronchi - Synesthesia alternate)
[comment]: # (* Henk Veldhuis - Huawei alternate)

Oniro Specification Committee Regrets:

* Davide Ricci - Huawei

Other Attendees:

* none

Eclipse Foundation:

 * Wayne Beaton
 * Agustin Benito Bethencourt
 * Paul Buck

## Minutes

### Approval of the meeting minutes from the 2022-06-21 2022wk25 Oniro WG Specification Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/mom-oniro-working-group-specification-committee-2022wk25.md>

[comment]: # (Commit considered: xxxxxx)

No objections, approved.


### Potential first spec project.

* Marta mentioned that there is a proposal focused on ONIRO interoperability that will be presented at the next meeting (with Davide present)
 

### Jakarta as example for Oniro
* Paul presented the activity of Jakarta and its structure in terms of specification. He overviewed also Sparkplug.



### Confirm availability during summer of the Oniro WG SpecC Member Reps.

Please advise during which of the tentative dates you would NOT be available:

Andrea Basso

| Date | Member |
| ------ | ------ | 
| July 5th | Yes/no | 
| July 19th | Yes/no |
| August 2nd | no |
| August 16th | no |
| August 30th | maybe |
| xxxx | Yes/no |

Davide R.

| Date | Member |
| ------ | ------ | 
| July 5th | no | 
| July 19th | Yes |
| August 2nd | Yes |
| August 16th | Yes |
| August 30th | Yes |
| xxxx | Yes/no |

Marta R. 

| Date | Member |
| ------ | ------ | 
| July 5th | Yes | 
| July 19th | Yes |
| August 2nd | Yes |
| August 16th | Yes |
| August 30th | Yes |
| xxxx | Yes/no |




### Resolutions

* Approval of agenda of previous meeting

#### AoB  (Open Floor)


## Links and references

### Links

This is the list of relevant links:
* Oniro WG SpecC MoM: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/tree/main/agenda-mom-specification-committee-oniro-wg>
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at 17:58
