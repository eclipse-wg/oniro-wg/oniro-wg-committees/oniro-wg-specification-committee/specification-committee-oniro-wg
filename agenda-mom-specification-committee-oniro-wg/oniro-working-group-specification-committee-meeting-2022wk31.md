# Oniro Working Group Specification Committee Meeting 2022wk31

MoM Status:  approved by the Oniro WG Specification Committee on 2022-08-16 commit d28eddc2c212e7f6ea640b6a85bff1cc67d4dc69

* Date: 2022-08-02
* Time: From 17:00 to 18:00 CEST
* Chair: Marta R.
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approval of the meeting minutes from the 2022-07-19 2022wk29 Oniro WG Specification Meeting - Marta R. (5 min)
* Oniro Specification project proposal outline - Marta R 25 min
* AsciiDoc for Oniro Specifications - Agustin and guest 15 min
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

* Marta Rybczynska - Representing Committers
* Andrea Basso - Synesthesia

Oniro Specification Committee Regrets:

Davide Ricci - Huawei

Other Attendees:

* none

Eclipse Foundation:

[comment]: # ( * Wayne Beaton)
* Agustin Benito Bethencourt
* Paul Buck

## Minutes

### Approval of the meeting minutes from the 2022-07-19 2022wk29 Oniro WG Specification Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/mom-oniro-working-group-specification-committee-2022wk29.md>

Commit considered: fccfe881c301d3f4aff3addfe8b87a2874455d6d 

No objections, approved.


### Oniro Specification project proposal outline

Martha presents the proposal outline

#### Parent project

All projects (except top-level projects) are nested under an existing parent project. You can leave this field blank initially, but it must be filled in before the project can be created. Before specifying a parent project, you should be sure to communicate your intent with that project's leadership. Note that the parent project may be the same as the top-level project.

#### Background

Optionally provide the background that has lead you to creating this project.

In the goal of defragmentation of the embedded/IoT space, Oniro needs to define a list of requirements such a device must meet to be called Oniro-compatible. This includes both hardware and software requirements.

#### Scope

All projects must have a well-defined scope. Describe, concisely, what is in-scope and (optionally) what is out-of-scope. A project cannot have an open-ended scope. 

In-scope: provide a list of requirements for a product to meet. If they do, they will seamlessly work with other Oniro devices.

Out-of-scope: this specification will not specify particular Oniro components for which it will lead to other specifications

#### Discussions

Two comments were made by Paul B.:
* the scope should include the TCK and the compatibility program
* The description should mention specifications instead of standards and it could make a reference to OpenHarmony, given that this project seems to be an umbrella project for many specifications.

Agustin:
* always describe the acronyms 

Next steps to be performed by Marta to get consensus on the proposal and submit the project proposal by committers:
* review the scope/description with the initial developers
* gather the initial developer team.
* then submit the proposal

### AsciiDoc for Oniro Specifications 

This presentation is postponed to a later date. Agustin is working to confirm an savvy invitee for the coming Oniro Wg SpecC session. 


### Resolutions

Approved unanimously the 2022wk29 MoM sha fccfe881c301d3f4aff3addfe8b87a2874455d6d

#### AoB  (Open Floor)

No topics

## Links and references

### Links

This is the list of relevant links:
* Oniro WG SpecC MoM: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/tree/main/agenda-mom-specification-committee-oniro-wg>
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at <17:42>
