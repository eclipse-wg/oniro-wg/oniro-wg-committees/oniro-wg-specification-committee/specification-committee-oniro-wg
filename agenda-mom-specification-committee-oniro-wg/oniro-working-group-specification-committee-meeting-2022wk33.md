# Oniro Working Group Specification Committee Meeting 2022wk33

MoM Status:  approved by the Oniro WG Specification Committee on 2022-11-22 commit 9a857c24fd7dc31f1ea2aea0da7b40877a6465b7

* Date: 2022-08-16
* Time: From 17:00 to 18:00 CEST
* Chair: Marta R.
* Minutes taker: Andrea Basso
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/89018431808?pwd=c0ZPdkxpU3ZGMHl1dmNHMmplZ004UT09>
   * Meeting ID: 890 1843 1808
   * Passcode: 475533

## Agenda

Agenda:
* Approval of the meeting minutes from the 2022-08-02 2022wk31 Oniro WG Specification Meeting - Marta R. (5 min)
* Oniro Specification project proposal outline - Marta R 20 min
* AsciiDoc for Oniro Specifications - Agustin and guest 15 min
* EclipseCon for the Oniro WG SpecC - Agustin 10 min 
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG Specification Committee

Oniro Specification Committee Members (Quorum =  2 of 3 for this Meeting):

* Marta Rybczynska - Representing Committers

* Davide Ricci - Huawei

Oniro Specification Committee Regrets:

 * Andrea Basso - Synesthesia

Other Attendees:

* none

Eclipse Foundation:

[comment]: # ( * Wayne Beaton)
* Agustin Benito Bethencourt
[comment]: # ( * Paul Buck)

## Minutes

### Approval of the meeting minutes from the 2022-08-02 2022wk31 Oniro WG Specification Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/blob/main/agenda-mom-specification-committee-oniro-wg/mom-oniro-working-group-specification-committee-2022wk31.md>

Commit considered:  d28eddc2c212e7f6ea640b6a85bff1cc67d4dc69

### Oniro Specification project proposal outline

* First version is on its way. Most people on vacation but we will push the committers proposal.
* License compliance should be included.
* The conversation on where to start the specs is a conversation we want to happen within the project among committers.
* What is our target audience? Pick a subset for now even if it is not explicit. Currently our main target audience are:
   * Content creators: we are not there with this one
   * ODMs: this could be an option
   * SoC/device makers: this could be an option
* Identify what is specific/great about Oniro at spec level.
   * OpenHarmony fulfills  a requirement. What is that requirement. Where can it help to Oniro.
   * Positioning Oniro - OpenHarmony at spec level. What does it mean that Oniro is openharmony compatible? Benefit? We need to answer this question.
* Once the committers are identified, bring them to the Oniro WG SpecC to talk about expectations.
   * This could be an agenda point for the coming SpecC.
* Agustin has been checking the OpenHarmony compatibility page. Some questions will arise about the process to have a solution made compatible.

### AsciiDoc for Oniro Specifications 

* Presentation done by Alex. Slides will be provided [FIXME] link to slides

### EclipseCon for the Oniro WG SpecC

* Oniro at EclipseCon 2022: general view
   * LInk to the EclipseCon 2022 wiki page: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon>
* f2f committee meetings.
   * Link to the f2f committee schedule: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon#5-oniro-committees-f2f-meetings>
   * Who is attending from the Oniro WG SpecC?
* EclipseCon 2022 for the Oniro WG SpecC.
   * Is it relevant to us to meet other SpecC members from other WGs to learn about what they are doing?
   * Could be good to have an structured section where there are status presentations plus Q&A?

### Resolutions

Approved unanimously the 2022wk31 MoM sha d28eddc2c212e7f6ea640b6a85bff1cc67d4dc69

#### AoB  (Open Floor)

* No AoB

## Links and references

### Links

This is the list of relevant links:
* Oniro WG SpecC MoM: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-specification-committee/specification-committee-oniro-wg/-/tree/main/agenda-mom-specification-committee-oniro-wg>
* Eclipse Foundation Specification Process #link <https://www.eclipse.org/projects/efsp/>
   * Eclipse Foundation Specification Operations Guide #link <https://www.eclipse.org/projects/efsp/operations.php>
* Oniro WG Specification discussion Mailing list #link: <https://accounts.eclipse.org/mailing-list/oniro-wg-specification>
* Oniro WG Specification Committee Mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-spec-committee>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SpecC: Specification Committee

Meeting is adjourned at <17:57>
